package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance = null;

	private final String URL;

	public static synchronized DBManager getInstance() {
		if(instance == null) instance = new DBManager();
		return instance;
	}

	private DBManager() {
		try {
			URL = Files.readString(Path.of("app.properties")).split("connection.url=")[1];
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		try (Connection conn = DriverManager.getConnection(URL)){
			Statement selectAll = conn.createStatement();
			ResultSet resultSet = selectAll.executeQuery("SELECT * FROM users");
			List<User> result = new ArrayList<>();
			while(resultSet.next()){
				User user = User.createUser(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				result.add(user);
			}
			selectAll.close();
			resultSet.close();
			return result;
		} catch (SQLException e) {
			throw new DBException("can't select", e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		int result;
		try (Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement insertStatement = conn.prepareStatement("INSERT INTO users VALUES(Default, ?)");
			insertStatement.setString(1,user.toString());
			result = insertStatement.executeUpdate();
			insertStatement.close();

			PreparedStatement selectStatement = conn.prepareStatement("select id from users where login = ?");
			selectStatement.setString(1, user.toString());
			ResultSet resultSet = selectStatement.executeQuery();
			while (resultSet.next()){
				user.setId(resultSet.getInt("id"));
			}
			selectStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			throw new DBException("can't insert", e);
		}

		return result == 1;
	}

	public boolean deleteUsers(User... users) throws DBException {
		int result = 0;
		for(User user : users){
			try(Connection conn = DriverManager.getConnection(URL)){
				PreparedStatement deleteStatement = conn.prepareStatement("DELETE FROM users WHERE login = ?");
				deleteStatement.setString(1, user.getLogin());
				result += deleteStatement.executeUpdate();
				deleteStatement.close();
			} catch (SQLException e) {
				throw new DBException("delete failed", e);
			}
		}
		return result > 0;
	}

	public User getUser(String login) throws DBException {
		try(Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement selectStatement = conn.prepareStatement("SELECT * from users WHERE login = ?");
			selectStatement.setString(1, login);
			ResultSet resultSet = selectStatement.executeQuery();
			if(resultSet.next()){
				User user = User.createUser(login);
				user.setId(resultSet.getInt("id"));
				return user;
			}
			selectStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			throw new DBException("select fail", e);
		}

		return null;
	}

	public Team getTeam(String name) throws DBException {
		try(Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement selectStatement = conn.prepareStatement("SELECT * from teams WHERE name = ?");
			selectStatement.setString(1, name);
			ResultSet resultSet = selectStatement.executeQuery();
			if(resultSet.next()){
				Team team = Team.createTeam(name);
				team.setId(resultSet.getInt("id"));
				return team;
			}
			selectStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			throw new DBException("select fail", e);
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		try (Connection conn = DriverManager.getConnection(URL)){
			Statement selectAll = conn.createStatement();
			ResultSet resultSet = selectAll.executeQuery("SELECT * FROM teams");
			List<Team> result = new ArrayList<>();
			while(resultSet.next()){
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				result.add(team);
			}
			selectAll.close();
			resultSet.close();
			return result;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		int result;
		try (Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement insertStatement = conn.prepareStatement("INSERT INTO teams VALUES(Default, ?)");
			insertStatement.setString(1,team.toString());
			result = insertStatement.executeUpdate();
			insertStatement.close();
			PreparedStatement selectStatement = conn.prepareStatement("select id from teams where name = ?");
			selectStatement.setString(1, team.toString());
			ResultSet resultSet = selectStatement.executeQuery();
			while (resultSet.next()){
				team.setId(resultSet.getInt("id"));
			}
			selectStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			throw new DBException("can't insert", e);
		}

		return result == 1;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		int idOfUser = user.getId();
		int result = 0;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL);
			conn.setAutoCommit(false);
			for (Team team :
					teams) {
				PreparedStatement insertUserTeams = conn.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
				insertUserTeams.setInt(1, idOfUser);
				insertUserTeams.setInt(2, team.getId());
				result += insertUserTeams.executeUpdate();
				insertUserTeams.close();
			}
			conn.commit();
			conn.setAutoCommit(true);

		} catch (SQLException e){
			try {
				conn.rollback();
				conn.setAutoCommit(true);
			} catch (SQLException e1) {
				throw new DBException("a", e1);
			}
			throw new DBException("failed" , e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				throw new DBException("close fail", e);
			}
		}
		return result > 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement selectTeams = conn.prepareStatement(
				"SELECT name, t.id from users_teams LEFT JOIN teams t on t.id = users_teams.team_id WHERE user_id = ?");
			selectTeams.setInt(1, user.getId());
			ResultSet rs = selectTeams.executeQuery();
			List<Team> result = new ArrayList<>();
			while(rs.next()){
				String name = rs.getString(1);
				Team team = Team.createTeam(name);
				team.setId(rs.getInt("id"));
				result.add(team);
			}
			selectTeams.close();
			rs.close();
			return result;
		} catch (SQLException e){
			throw new DBException("failed", e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		int result = 0;
		try(Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement deleteTeam = conn.prepareStatement("DELETE FROM teams where name = ? and id = ?");
			PreparedStatement deleteRelation = conn.prepareStatement("DELETE from users_teams where team_id = ?");
			deleteTeam.setString(1, team.getName());
			deleteTeam.setInt(2, team.getId());
			deleteRelation.setInt(1, team.getId());
			result += deleteTeam.executeUpdate();
			result += deleteRelation.executeUpdate();
			deleteTeam.close();
			deleteRelation.close();
		} catch (SQLException e){
			throw new DBException("failed", e);
		}
		return result > 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		int result = 0;
		try(Connection conn = DriverManager.getConnection(URL)){
			PreparedStatement updateTeam = conn.prepareStatement("UPDATE teams SET name = ? where id = ?");
			updateTeam.setString(1, team.getName());
			updateTeam.setInt(2, team.getId());
			result += updateTeam.executeUpdate();
			updateTeam.close();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}		return result > 0;
	}

}
